{spawn} = require 'child_process'
fs = require 'fs'

exec = (args, callback) ->
	cmd = spawn('heroku', args)
	out = null
	err = null

	cmd.stdout.on 'data', (buffer) =>
		out = (out or '') + buffer.toString()

	cmd.stderr.on 'data', (buffer) =>
		err = (err or '') + buffer.toString()

	cmd.on 'close', () =>
		if callback
			callback(err, out)

class HeroConf
	constructor: (@file, @prefix) ->
		try
			@config = require @file
		catch e
			# nope

	get: (key) ->
		herokuKey = key

		if @prefix
			herokuKey = @prefix + '_' + key

		if process.env[herokuKey]
			return process.env[herokuKey]

		if @config and @config[key]
			return @config[key]

		throw new Error "Could not find key '" + (herokuKey or key) + "'"

	clean: (callback) ->
		args = ['config:unset']

		for key, value of @config
			if @prefix
				key = @prefix + '_' + key

			args.push(key)

		exec(args, callback)

		@

	push: (callback) ->
		args = ['config:set']

		for key, value of @config
			if @prefix
				key = @prefix + '_' + key

			args.push(key+'='+value)

		exec(args, callback)

		@

	pull: (callback = ->) ->
		@getHerokuConfig (err, config) =>
			@config = @config or {}
			console.log config

			for key, value of config
				if !@prefix
					@config[key] = value
				else if @prefix and key.indexOf(@prefix) == 0
					@config[key.replace(@prefix+'_', '')] = value

			file = @file
			if file.indexOf('.json') == -1
				file += '.json'

			console.log @config

			fs.writeFileSync file, JSON.stringify(@config, null, 2)

			callback(err)

		@

	getHerokuConfig: (callback) ->
		@getHerokuConfigRaw (err, raw) =>
			callback(err, @parseHerokuConfig(raw))

	getHerokuConfigRaw: (callback) ->
		exec(['config'], callback)

	parseHerokuConfig: (raw) ->
		raw = raw.split('\n')
		raw.shift()

		config = {}

		for line in raw
			if line
				parts = line.split(':')
				key = parts.shift()
				value = parts.join(':').trim()

				config[key] = value

		return config

	destroy: (callback) ->
		args = ['config:unset']
		
		@getHerokuConfig (err, config) ->
			if err
				return callback(err, null)

			for key, value of config
				if key != 'PATH'
					args.push(key)

			exec(args, callback)

		@

module.exports = {
	require: (file, prefix) ->
		return new HeroConf(file, prefix)

	destroy: (callback) ->
		return new HeroConf().destroy(callback)
}