heroconf = require './heroconf'
glob = require 'glob'

print = (err, out) ->
	if err 
		console.log err
	if out 
		console.log out

cli =
	push: (pattern, prefix) ->
		if !pattern
			return console.log "Push needs a glob pattern<3"

		glob pattern, (err, files) ->
			if err
				return console.log err

			files.forEach (file) ->
				config = heroconf.require(process.cwd() + '/' + file, prefix)
				config.push print

	pull: (path, prefix) ->
		if !path
			return console.log "Pull needs a file path<3"

		config = heroconf.require(process.cwd() + '/' + path, prefix)
		config.pull print

	clean: (pattern, prefix) ->
		if !pattern
			return console.log "Clean needs a glob pattern<3"

		glob pattern, (err, files) ->
			if err
				return console.log err

			files.forEach (file) ->
				config = heroconf.require(process.cwd() + '/' + file, prefix)
				config.clean print

	destroy: () ->
		heroconf.destroy print

if process.argv.length < 3
	return console.log "Need more args<3"

cmd = process.argv[2]
args = process.argv.slice(3, process.argv.length)

if !cli[cmd]
	return console.log "Command #{cmd} does not exist<3"

cli[cmd].apply(@, args)